#include "queue.h"

#include <stdlib.h>

static const size_t size = 20;

void queue_init(struct queue * v, size_t alloc_size)
{
    v->values = malloc(alloc_size * size); // HD to 20
    v->head = v->values;
    v->tail = v->values;
}

void queue_destroy(struct queue * v)
{
    free(v->values);
    v->values = NULL;
    v->head = NULL;
    v->tail = NULL;
}

void queue_enqueue(struct queue * v, void * value)
{
    v->tail = value;
    if (v->tail == (v->values + (size - 1) * v->alloc_size) )
    {
        v->tail = 0;
    }
    else
    {
        v->tail += 1;
    }
    // TODO error checking
}
