﻿#include "bst.h"

#include <stdlib.h>

static struct bst_node* init_node(void *key)
{
    struct bst_node* node = malloc(sizeof(struct bst_node));
    node->left = NULL;
    node->right = NULL;
    node->parent = NULL;
    node->key = key;
    return node;
}

static void transplant(struct bst_root *tree, struct bst_node *u, struct bst_node *v)
{
    if (u->parent == NULL)
        tree->root = v;
    else if (u == u->parent->left)
        u->parent->left = v;
    else
        u->parent->right = v;

    if (v != NULL)
        v->parent = u->parent;
}

static struct bst_node* bs_tree_maximum_impl(struct bst_node* x)
{
    while (x->right != NULL)
        x = x->right;

    return x;
}

static struct bst_node* bs_tree_minimum_impl(struct bst_node* x)
{
    while (x->left != NULL)
        x = x->left;
    return x;
}

void bst_init(struct bst_root *tree, cmp_fun_t cmp)
{
    tree->root = NULL;
    tree->cmp = cmp;
}

void bst_destroy(struct bst_root *tree)
{
    // should traverse the entire tree
    // and destroy each node

    struct bst_iterator iter;
    bst_iterator_init(tree, &iter);

    while (bst_iterator_advance(&iter))
    {
        free(iter.current);
    }

    tree->cmp = NULL;
    tree->root = NULL;
}

void bst_insert(struct bst_root *tree, void *key)
{
    struct bst_node *y = NULL;
    struct bst_node *x = tree->root;
    struct bst_node *z = init_node(key);
    cmp_fun_t cmp = tree->cmp;

    while (x != NULL)
    {
        y = x;
        if (cmp(z->key, y->key) < 0)
            x = x->left;
        else
            x = x->right;
    }
    z->parent = y;
    if (y == NULL)
        tree->root = z;
    else if (cmp(z->key, y->key) < 0)
        y->left = z;
    else
        y->right = z;
}

void bst_remove(struct bst_root *tree, struct bst_node *z)
{
    if (z->left == NULL)
        transplant(tree, z, z->right);
    else if (z->right == NULL)
        transplant(tree, z, z->left);
    else
    {
        struct bst_node *y = bs_tree_minimum_impl(z->right);
        if (y->parent != z)
        {
            transplant(tree, y, y->right);
            y->right = z->right;
            y->right->parent = y;
        }
        transplant(tree, z, y);
        y->left = z->left;
        y->left->parent = y;
    }
}

struct bst_node* bst_search(struct bst_root *tree, void *element)
{
    struct bst_node* x = tree->root;
    cmp_fun_t cmp = tree->cmp;

    while (x != NULL && !cmp(element, x->key))
    {
        if (cmp(element, x->key))
            x = x->left;
        else
            x = x->right;
    }
    return x;
}

struct bst_node* bst_minimum(struct bst_root *tree)
{
    struct bst_node *x = tree->root;
    if (x == NULL)
        return NULL;
    return bs_tree_minimum_impl(x);
}

static void bst_inorder_walk_impl(struct bst_node* node, callback_fun_t cb, void* state)
{
    if (node != NULL)
    {
        bst_inorder_walk_impl(node->left, cb, state);
        cb(node->key, state);
        bst_inorder_walk_impl(node->right, cb, state);
    }
}

int bst_inorder_walk(struct bst_root *tree, callback_fun_t cb, void *state)
{
    if (tree != NULL && tree->root != NULL)
    {
        bst_inorder_walk_impl(tree->root, cb, state);
        return 1;
    }
    return 0;
}

struct bst_node* bst_maximum(struct bst_root *tree)
{
    struct bst_node *x = tree->root;
    if (x == NULL)
        return NULL;
    return bs_tree_maximum_impl(x);
}

int bst_iterator_init(struct bst_root *tree, struct bst_iterator *iterator)
{
    return 1;
}

int bst_iterator_advance(struct bst_iterator *tree)
{
    return 0;
}

