#include "gtest/gtest.h"

#include "queue.h"

#include <memory>

using std::make_unique;

TEST(QUEUE_TEST, CreateTest)
{
    queue v;
    queue_init(&v, sizeof(int));
}

TEST(QUEUE_TEST, EnqueueTest)
{
    queue v;
    queue_init(&v, sizeof(int));
    auto x1 = make_unique<int>(3);
    queue_enqueue(&v,x1.get());
}

TEST(QUEUE_TEST, DestroyTest)
{
    queue v;
    queue_init(&v, sizeof(int));
    auto x1 = make_unique<int>(3);
    queue_enqueue(&v,x1.get());
    queue_destroy(&v);
}
