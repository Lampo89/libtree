#include "bst.h"

#include <stdlib.h>
#include <stdio.h>

static int cmp_integers(void* el1, void* el2)
{
    return *((int*)el1) < *((int*)el2);
}

static void sum_of_keys(void* el, void* state)
{
    int* int_el = (int*)el;
    int* int_state = (int*)state;
    *int_state += *int_el;
}

static void print_keys(void* el, void* state)
{
    printf("%i ", *(int*)el);
}

int main()
{
    // will contain ints
    struct bst_root tree;

    bst_init(&tree, cmp_integers);

    for (int i = 0; i < 10; ++i)
    {
        int* el = (int*)malloc(sizeof(int));
        *el = 2 * i + 1;
        bst_insert(&tree, el);
    }

    struct bst_node *max = bst_maximum(&tree);
    struct bst_node* min = bst_minimum(&tree);

    int sum_value = 0;
    bst_inorder_walk(&tree, sum_of_keys, &sum_value);
    bst_inorder_walk(&tree, print_keys, NULL);
   
    printf("\n");
    printf("Max = %i\n", *((int*)max->key));
    printf("Min = %i\n", *((int*)min->key));
    printf("SumOfKeys = %i\n", sum_value);

    // remove the max
    bst_remove(&tree, max);

    max = bst_maximum(&tree);
    printf("Max = %i", *((int*)max->key));

    struct bst_iterator it;
    bst_iterator_init(&tree, &it);

    // tear down
    while (bst_iterator_advance(&it))
    {
        free((int*)it.current->key);
    }

    bst_destroy(&tree);
}

