#include "gtest/gtest.h"

#include "bst.h"

static int cmp_integers(void* el1, void* el2)
{
    return *((int*)el1) < *((int*)el2);
}

TEST(BST_TEST, InsertTest)
{
    bst_root tree;
    bst_init(&tree, cmp_integers);
    int value = 1;
    bst_insert(&tree, &value);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
