#ifndef BST_H
#define BST_H

typedef int (*cmp_fun_t)(void* el1, void* el2);
typedef void (*callback_fun_t)(void* el, void* state);

struct bst_node
{
    void *key;
    struct bst_node *left, *right, *parent;
};

struct bst_root
{
    struct bst_node* root;
    cmp_fun_t cmp;
};

//
// Binary search tree iterator
//

struct bst_iterator
{
    struct bst_node* current;
};

#ifdef __cplusplus
extern "C" {
#endif // !_cplusplus

/**
 * Initialize the binary search tree
 */
void bst_init(struct bst_root* tree, cmp_fun_t cmp);

/**
 * Destroy the binary search tree
 */
void bst_destroy(struct bst_root* tree);

void* bst_get_key(struct bst_node* node);

/**
 * Insert a given element in the binary search tree
 */
void bst_insert(struct bst_root* tree, void* element);

/**
 * Remove a node from the binary search tree.
 * Assumes that node effectively belongs to the tree.
 */
void bst_remove(struct bst_root* tree, struct bst_node* node);

void bst_remove_key(struct bst_root* tree, void* key);

/**
 * Search
 */
struct bst_node* bst_search(struct bst_root* tree, void* element);

struct bst_node* bst_maximum(struct bst_root* tree);

struct bst_node* bst_minimum(struct bst_root* tree);

//
// in-order walk
//
int bst_inorder_walk(struct bst_root* tree, callback_fun_t cb, void* state);

int bst_iterator_init(struct bst_root* tree, struct bst_iterator* iterator);
int bst_iterator_advance(struct bst_iterator* iterator);

#ifdef __cplusplus
}
#endif // !_cplusplus


#endif
