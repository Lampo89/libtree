#ifndef QUEUE_H
#define QUEUE_H

#include <stddef.h>

struct queue
{
    void * values;
    void * head;
    void * tail;
    size_t alloc_size;
};

#ifdef __cplusplus
extern "C" {
#endif // !_cplusplus

void queue_init(struct queue * v, size_t alloc_size);

void queue_destroy(struct queue * v);

void queue_enqueue(struct queue * v, void * value);

#ifdef __cplusplus
}
#endif // !_cplusplus


#endif
